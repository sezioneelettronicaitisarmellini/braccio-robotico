#include<Servo.h>
bool automatic = true;
Servo motors[5];
uint8_t relax[5] = {120, 160, 75, 65, 50};
uint8_t movimento[][2] = {{4, 10},
  {0, 60},
  {4, 90},
  {0, 120},
  {2, 10},
  {0, 60},
  {4, 10},
  {0, 120},
  {0, 60},
  {4, 90},
  {0, 120},
  {2, 75},
  {0, 60},
  {4, 10},
  {0, 120},
  {4, 50}
};
uint8_t m[][2] = {
  {4, 20},
  {0, 60},
  {4, 70},
  {3, 150},
  {0, 120},
  {2, 30},
  {0, 60},
  {3, 65},
  {4, 20},
  {0, 120},
  {3, 150},
  {0, 80},
  {3, 65},
  {0, 60},
  {4, 70},
  {0, 120},
  {2, 75},
  {0, 60},
  {4, 20},
  {0, 120}
};


constexpr int size = sizeof(movimento) / sizeof(movimento[1]);
constexpr uint8_t button = 7;
void setup() {
  pinMode(button, INPUT_PULLUP);
  Serial.begin(9600);
  set_position(relax);
  motors[0].attach(11); //Motore 1
  motors[1].attach(10); //Motore 2
  motors[2].attach(9); //Base
  motors[3].attach(6); //Polso
  motors[4].attach(5); //Pinza
}

void loop() {
  if (automatic) {
    step(movimento);
    delay(5000);
  }
  if (Serial.available() > 0) {
    char c = Serial.read();
    if (c == 'M') {
      int n = Serial.parseInt();
      Serial.read();
      int x = Serial.parseInt();
      Serial.print("Motore ");
      Serial.print(n);
      Serial.print(" a ");
      Serial.println(x);
      motors[n].write(x);
    }
    if (c == 'S') {
      automatic = false;
    }
    if (c == 'V') {
      automatic = true;
    }
  }
}
void set_position(uint8_t* a) {
  for (int i = 0; i < 5; i++) {
    motors[i].write(a[i]);
    delay(10);
  }
}
template<typename T>
void step(T a) {
  for (int i = 0; i < size; i++) {
    int pos = motors[a[i][0]].read();
    int fin = a[i][1];
    int step = (fin - pos) / 10;
    for (int k = 0; k < 10; k++) {
      pos += step;
      motors[a[i][0]].write(pos);
      switch (a[i][0]) {
        case 4:
          delay(30);
          break;
        default:
          delay(45);
      }
    }
    delay(300);
  }
}
